---
title: "Home"
draft: false
date: 2020-11-01T13:15:03+08:00
---

I am a software engineer with many years of experience building and maintaining web applications.
I am based in Taiwan where I completed my education in computer science at the bachelor's level and communications engineering at the master's level.

Previously a masters student in the Wireless Communications Information SecurityLab (WCISLAB) at the National Tsing Hua University, Communications Institute,
under the supervision of [Scott Huang](https://www.ee.nthu.edu.tw/chhuang/).

<!-- {{<figure src="images/portrait.png" class="portrait">}} -->


### Also on

[Gitlab](https://gitlab.com/th3lusive) |
[Github](https://github.com/BrandonBenn)
