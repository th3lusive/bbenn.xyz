---
title: "Three things I can do for myself, my family, and society"
date:  "2020-10-28"
slug: three-things
draft: false
---

# Three Things I Can Do For Myself

## Understand Self

Before examining myself through my desires and intuitions, I toiled for
many years trying to understand myself as a person, and my place in this
world. I was worried by the fact that I didn\'t understand what my
purpose in life is, and what I should do to attain it. Whenever I tried
to define my purpose in life, it always seemed mundane, vague, or
obtuse. This has forced me to introspect. What I realized is that there
was no specific goal I wanted to achieve, but to attain an understanding
of self. I came to the conclusion that once this is attained, whatever
goals I want to achieve, as well as the intrinsic motivation to execute
them.

> "The unexamined life is not worth living." --- Socrates.

The more I introspect to understand my base desires and intuitions, the
more that quote by Socrates resonates with me. From personal experience,
whenever I think I have gained understanding (which is always
tentative), something about myself that I didn't before, I get the
feeling of satisfaction. Then with this newfound understanding, I try to
reinterpret my base desires and intuitions, so as to refine and redefine
my purpose. What I want to do to be at peace with oneself, through a
rigorous interrogation of one's desires and intuitions, as well as
external state of affairs and gradually make them align with each other,
so as to reduce friction between them, and ultimately to pursue all of
these desires.

While I am not convinced that there will be a point when one finally
understands themselves, what I am convinced of is the fact that the
endeavor itself will be a fulfilling experience.

## Be Skeptical Open Minded

The second thing I feel obligated to myself to do is to always be open
minded. Throughout my life I have been exposed to a myriad of different
views, beliefs, and ideas. Some of which at some point I have been
convinced are true. Being completely honest with oneself, one would
realize that there are beliefs that I hold that are certainly false.
Another fact is that I don\'t know which of them are false. So the
implication of these two inescapable points are that I always must be
ready and willing to accept the fact that I may be wrong about a number
of things. This in my view is the guiding principle of scepticism that
for my own benefit, I hope to adopt in life whenever I am presented with
an idea that I have to seriously consider.

## Exercise Discipline

Lastly, the third obligation to myself is to be disciplined. One common
definition of discipline would be, to train or develop by instruction
and exercise especially in self-control. This is partially what I am
referring to, however I would prefer to phrase it as; doing what I am
supposed to do, when I\'m supposed to do it, regardless of what I am
feeling in that moment. I am not talking about some desire to endure
pain, but the ability to recognize that what I want to achieve in life
isn\'t easy, and I must be willing to sacrifice in order to get it. I
must not ignore the forest while looking at the tree. The things that I
am doing now all play a role in the bigger scheme of things.

# Three Things I Can Do For My family

## Show gratitude to parents

My family did their best to provide for me and my siblings. Both of my
parents sacrificed greatly for our benefit and they did their best to
bring us up in the way they thought was right, to instill their values and
morals onto us. Despite their various shortcomings, I am still very
grateful for all they have done for us as children, and for all they are
still doing for us now as adults. Now as an adult, I feel indebted to my
parents to assure them that all those years of sacrifice and hard work
paid off, and now that I am everything they hoped I would be, and
perhaps even more.

As a child, I didn\'t feel any pressure to meet their standard as to
what was expected of me, but based on what they tell me now in
adulthood, it would seem as though I have somewhat met if not all, but
most of their expectations of me. My mother is just relieved by the fact
that I eventually pulled myself together after high school because
according to their own account, I was an uninspired student. My father
is one of ten children, and my mother, one of just two. Between both
sides of my parents\' siblings\' children, my older brother and my
sisters are the only ones to have completed at least a bachelor\'s
degree at university. My father on the other hand is proud to remind
everyone of this fact.

By continuing to work hard and to put in to practice all of the values
taught by my family and upbringing I am showing gratitude to them by
letting them know that I appreciate everything they have taught me and I
am a better person for it.

## Take Care Of Family

For most of my parents\' lives they have been relatively active, and
healthy, but in recent years watching them become old has been
depressing. It is a reminder of how fleeting life is and that every
moment of it must be cherished. As my parents grow in age I hope to be
able to ease their discomfort, and allow them to enjoy life. I want to
be able to provide for them now, as they did when I was a child.

I am very close with my siblings, and I feel indebted to ensure that
their well-being is maximized. When I can, I sacrifice for them, and I
work towards being able to have the stability to extend that even
future. Some of them already have children and I want to be in their
children\'s lives as well.

## Prepare For Future Family

I hope to one day have children of my own. I would like them to have the
upbringing I had, and what I wished I had as well. I hope to instill in
them values and morals that I think are important, such as duties to
themselves and others, how to practice love, to be humble, and to
question everything.

# Three Things I Can Do For Society

## Improve Quality of Life

As someone who moved abroad in hopes of furthering my education, and
ultimately, my career, the question of what I want to accomplish in life
is always recurring. We are shaped by the environment we grow up in and
this plays a big role in what we value, so depending on where one grows
up and the social and economic conditions play an important role. I am
from St. Vincent and the Grenadines, a small country in the southern
caribbean. Although not a poor country, there is much to be desired in
terms of infrastructure, chiefly technology.

From a very young age, I\'ve always been fascinated by technology and
the effect that it has on our lives. As a teenager I started dabbling in
my own little technological projects that at the time I thought would
become a product that people would want to use. At the end of high
school, I didn\'t know what the next move would be for me, as my family
couldn\'t afford to send me off to university, and my grades were short
of exceptional so I doubted being able to get a scholarship. So I
applied for an internship at the National Telecommunications Regulatory
Commission. During that time I learned a lot about the
telecommunications infrastructure in our country and was disappointed in
how poor it was, despite being so important to the everyday lives of the
people in my country.

Contrary to when I got to Taiwan, I was blown away by how developed the
country was, despite being significantly worse off only a few decades
ago. I was also impressed by how accessible the internet was and how all
of the services that relied on it made the lives of the citizens so much
more convenient. I wanted to study engineering and Taiwan was the
perfect place to do so because of how well developed they are in that
regard. I had a desire that one day, using Taiwan as a model example,
improved the technological infrastructure of my own country, providing a
platform by which all other technological innovations can be built upon
so as to provide the convenience that can improve the quality of life.

## Mentor Youth

Much of the problems that still plague our societies stem from a lack of
knowledge or understanding. By mentoring people younger than myself, and
imparting knowledge we are able to break the mind-forged manacles that
stifle the progression of society. Knowledge is indeed power. I hope to
teach people who are not exposed to certain information and insight,
also in the process, further elucidating my own knowledge.

## Support Those In Need

By virtue of the time, place, social and economic environment, we are
all born into different circumstances and later on in life, much of the
good fortune that falls upon us is also out of our control. In light of
this fact we should always be empathetic to others, especially those in
need and where possible to support them. We can be very wasteful with
our spending at times, some of which can be allocated to supporting
groups or individuals who are in need of financial assistance. Donating
is a worthwhile endeavor that would give me a great deal of
satisfaction.

Also supporting people who are oppressed and discriminated against. Be
it kids who are bullied, women who go through harassment from their
spouses or in-laws, be it the poor or mentally challenged who are looked
down upon by others in society. I will speak out for those who are in
need in an effort to strengthen human solidarity.

