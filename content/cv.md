---
title: "CV"
date: 2020-11-01T13:15:03+08:00
menu: "main"
weight: 3
draft: false
---

*Last updated on 2023-02-18* | [PDF](https://bbenn.xyz/cv.pdf)

## EDUCATION

**2020 - 2022** --- MSc, Communications Engineering, National Tsing Hua University.

**2015 - 2019** --- BSc, Computer Science, National Tsing Hua University.

**2014 - 2015** --- Chinese Enrichment Program, National Taiwan Normal University.

## EXPERIENCE

**2020 - 2021** --- Teaching Assistant

- **EE23100**: Introduction to Programming.

- **COM533500**: Network Security.

**2019 - 2020** --- Research Assistant

> Wireless Communications and Information Security (WCIS) research group.

## PROJECTS

**2020.01 - 2020.02** --- Election DApp

> A simple Ethereum DAPP Voting Application.

**2019.06 - 2019.08** --- Hyper-ledger Fabric Database Query Layer

with Thattapon Surasak, NTHU

> Implemented database query layer on top of hyper-ledger fabric
 database to optimize blockchain query times.

**2018.06 - 2019.01** --- QEMU Virtualization Optimization

with Bijon Setyawan Raja, NTHU

> Derived Lightweight QEMU conﬁguration w/ customized lightweight
GNU/Linux environment that boots in 3~ seconds and has image size of 5MB.

## ACHIEVEMENTS

**2018.03** --- Annual Hult Prize Regional Final

> Sponsored by Hult International Business School,
the Hult Prize benefitsfrom the business schools
global footprint, research and global resources.

**2014 - 2019** --- Taiwan Ministry of Foreign Aﬀairs (MOFA) Scholarship

> Aims to encourage outstanding international students to study in
Taiwan, as well as to promote bilateral exchanges and friendship
between the peoples of Taiwan and its diplomatic allies.

## SKILLS

**TECHNICAL** --- Ruby | Go | C/C++ | Python | Javascript | HTML/CSS | VueJS | ReactJS | Linux | Git | Shell/POSIX | LaTeX

**LANGUAGES** --- English (Native) | Chinese (Proficient)
